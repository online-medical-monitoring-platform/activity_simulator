package com.rabbitmq.rabbitmq;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SendService {

    private final static String QUEUE_NAME = "queue_broker";
    private static int activityCount = 0;
    private ConnectionFactory factory;
    private ObjectMapper objectMapper;

    @Scheduled(fixedDelay = 1000)
    private void sendMessages() throws Exception{
        ProcessData processData = new ProcessData();
        List<Activity> activities = processData.readData();

        factory = new ConnectionFactory();
        //factory.setHost("localhost");
        factory.setHost("squid.rmq.cloudamqp.com");
        factory.setUsername("yznmlowd");
        factory.setPassword("3gSTmvZMon__unCtWL50cpfE28JXZCCq");
        factory.setVirtualHost("yznmlowd");
        /*factory.setHost("kangaroo.rmq.cloudamqp.com");
        factory.setUsername("rjuznxbd");
        factory.setPassword("wOwrNnlQlYHBmx6Szu0UwZglIOU4EddX");
        factory.setVirtualHost("rjuznxbd");*/
        objectMapper = new ObjectMapper();

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = objectMapper.writeValueAsString(activities.get(activityCount++));
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println("[X] Sent client: " + message );
        }
    }


}
