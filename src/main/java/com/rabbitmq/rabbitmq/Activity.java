package com.rabbitmq.rabbitmq;

public class Activity {

	private Long patient_id;
	private String start;
	private String end;
	private String activity;



	@SuppressWarnings("deprecation")
	public Activity(String s, String e, String a) {
		super();
		start=s;
		end=e;
		activity=a;
		patient_id= Long.valueOf(3);
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Long getPatientId() {
		return patient_id;
	}

	public void setPatientId(Long patientId) {
		this.patient_id = patientId;
	}


	@Override
	public String toString() {
		return "{" +
				"patientId=" + patient_id +
				", start='" + start + '\'' +
				", end='" + end + '\'' +
				", activity='" + activity + '\'' +
				'}';
     /*   return "{"
                + "activity='" + activity + '\''
                + ", start=" + start
                +  ", end=" + end
                +  ", patientId=" + patient_id +
                '}';*/
	}
}
