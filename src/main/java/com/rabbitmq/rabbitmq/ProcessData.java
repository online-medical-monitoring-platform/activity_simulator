package com.rabbitmq.rabbitmq;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ProcessData {

	private List<Activity> inputData = new ArrayList<Activity>();

	// read line by line using Stream
	public List<Activity> readData() {
		
		Object[] lines = null;
		
		try (Stream<String> stream = Files.lines(Paths.get("activity.txt"))) {
		
			lines = stream.toArray();
		
		} catch (IOException e) {
		
			e.printStackTrace();
		
		}
		
		for(Object row: lines) {
			String line = row.toString();
			
			String[] aux = line.split("\t\t");
			
			Activity activity = new Activity(aux[0], aux[1], aux[2]);
			activity.setActivity(activity.getActivity().replaceAll("\\s",""));
			
			inputData.add(activity);
			
		}
		return inputData;
	}
	
}
